import angular from 'angular';
import './styles/styles.scss';

var myApp = angular.module('app', []);
var req_products = {
	method: 'GET',
	url: "http://localhost:8888/fv_test/index.php/rest/V1/products?searchCriteria",
	headers: {
		'Content-Type':'application/json',
		'Authorization' : 'Bearer bc464ufplr0rxxt7uxq50t0a21zmg1w8'
	}
}

myApp.controller('MainController', ['$scope', '$http', function($scope, $http) {
	$scope.breadcrumbs = "Home > Men > Shirts"
	$http(req_products).then(function successCallback(response) {
		$scope.items = response.data.items;
	}, function errorCallback(response) {
		console.log(response);
	});
}]);