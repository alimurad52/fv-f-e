## Project Setup

1. Clone the repository.

2. Open your terminal and navigate to the folder location.

3. In your terminal run the command `npm run serve`

4. In your browser navigate to `localhost:8080/`
